import {
  findMaxBidPlayer,
  isPlayerWinning,
  findMaxBidPlayerAndSecondMax,
  Player
} from "./players";
import { findWinningPriceGivingAReservePrice } from "./prices";

const main = () => {
  const salingItem = {
    reservePrice: 100
  };
  const players: Player[] = [
    {
      id: "A",
      bids: [110, 130]
    },
    {
      id: "B",
      bids: []
    },
    {
      id: "C",
      bids: [125]
    },
    {
      id: "D",
      bids: [105, 115, 90]
    },
    {
      id: "E",
      bids: [132, 135, 140]
    },
    {
      id: "F",
      bids: [50, 40, 20]
    },
    // first special: if E & G both win the auction we should define a strategy on which one win
    {
      id: "G",
      bids: [140]
    },
    {
      id: "H",
      bids: [150]
    }
  ];

  const player = findMaxBidPlayer(players);
  if (isPlayerWinning(player, salingItem.reservePrice)) {
    console.log(`Player winning is: ${player && player.id}`);
    console.log(
      `Player ${player &&
        player.id} has to pay ${findWinningPriceGivingAReservePrice(
        players,
        salingItem.reservePrice
      )}`
    );
  } else {
    console.log("No winning player");
  }
  // Alternative version to get the winning player and second max in one shot
  console.log("\n\n### Alternative version ###");
  console.log(JSON.stringify(findMaxBidPlayerAndSecondMax(players), null, 2)); // /!\ check the reserve price etc. /!\
};

main();
