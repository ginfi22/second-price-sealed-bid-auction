import {
  findMaxBidPlayer,
  isPlayerWinning,
  findMaxBidPlayerAndSecondMax
} from "./players";

describe("players module", () => {
  it("should find a highest bid within players", () => {
    const players = [
      {
        id: "A",
        bids: [100]
      },
      {
        id: "B",
        bids: [120]
      }
    ];
    expect(findMaxBidPlayer(players)).toEqual({
      id: "B",
      bids: [120]
    });
  });

  it("should not find a highest bid within players", () => {
    let players: any = null;
    expect(findMaxBidPlayer(players)).toBeFalsy();
    players = [];
    expect(findMaxBidPlayer(players)).toBeFalsy();
  });

  it("should find the player win the object giving the reserve price", () => {
    let player = {
      id: "B",
      bids: [120]
    };
    const reservePrice = 100;
    expect(isPlayerWinning(player, reservePrice)).toBe(true);
    player = {
      id: "B",
      bids: [100]
    };
    expect(isPlayerWinning(player, reservePrice)).toBe(true);
  });

  it("should find the player not win the object giving a reserve price", () => {
    const player = {
      id: "B",
      bids: [90, 80]
    };
    const reservePrice = 100;
    expect(isPlayerWinning(player, reservePrice)).toBe(false);
  });

  it("should find the highest biding player and second max price within players", () => {
    const players = [
      {
        id: "A",
        bids: [100]
      },
      {
        id: "B",
        bids: [120]
      }
    ];
    expect(findMaxBidPlayerAndSecondMax(players)).toEqual({
      secondMax: 100,
      maxBidPlayer: {
        id: "B",
        bids: [120]
      }
    });
  });
});
