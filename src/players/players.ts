export interface Player {
  id: string;
  bids: number[];
}

/**
 * Get the player who makes the best offer.
 * @param {Player[]} players The list of players trying to get the item
 * @return {Player | null} The player that makes the best offer or null
 */
export const findMaxBidPlayer = (players: Player[]): Player | null => {
  let maxBidPlayer = null;
  let maxBid = -1;

  if (!players || players.length === 0) {
    return null;
  }
  players.map(player => {
    let currentMaxBidPlayer = Math.max(...player.bids);
    if (currentMaxBidPlayer >= maxBid) {
      maxBidPlayer = player;
      maxBid = currentMaxBidPlayer;
    }
  });
  return maxBidPlayer;
};

/**
 * Check if the player win regarding the reservePrice.
 * @param {Player} player A player
 * @param {number} reservePrice The reserve price of the item
 * @return {true | false} True the player win. False the player loose
 */
export const isPlayerWinning = (
  player: Player | null,
  reservePrice: number
) => {
  if (!player || Math.max(...player.bids) < reservePrice) {
    return false;
  }
  return true;
};

/**
 * Get the player who makes the best offer and the second max price.
 * First sort prices and then go trough just once to get max bid and second max price.
 * @param {Player[]} players The list of players trying to get the item
 * @return {Object} The player that makes the best offer and the second max price
 */
export const findMaxBidPlayerAndSecondMax = (players: Player[]) => {
  let maxBidPlayer: Player | null = null;
  let maxBid = -1;
  let secondMax: number | null = null;

  if (!players || players.length === 0) {
    return null;
  }
  players.map(player => {
    let currentMaxBid = Math.max(...player.bids);
    if (currentMaxBid >= maxBid) {
      secondMax = maxBid < currentMaxBid ? maxBid : secondMax;
      maxBidPlayer = player;
      maxBid = currentMaxBid;
    }
  });
  return {
    secondMax,
    maxBidPlayer
  };
};
