import { Player } from "../players";

/**
 * Return the second maximum value givin a list of values.
 * @param {number[]} maxBidByPlayers Max bids list for players
 * @return {number} The second last max value
 */
export const getTheSecondLastMax = (
  maxBidByPlayers: number[]
): number | null => {
  if (!maxBidByPlayers || maxBidByPlayers.length === 0) {
    return null;
  }
  if (
    maxBidByPlayers[maxBidByPlayers.length - 2] <
    maxBidByPlayers[maxBidByPlayers.length - 1]
  ) {
    return maxBidByPlayers[maxBidByPlayers.length - 2];
  }
  maxBidByPlayers.pop();
  return getTheSecondLastMax(maxBidByPlayers);
};

/**
 * Finding the winning price means to get the max bid by players
 * and then get the last second max.
 * @param {Player[]} players List of players trying to win
 * @returns {number} The winning price givin players
 */
export const findWinningPrice = (players: Player[]) => {
  if (!players || players.length === 0) {
    return null;
  }
  const maxBidByPlayers = players
    .map(player => {
      return Math.max(...player.bids);
    })
    .filter(price => !!price)
    .sort((x, y) => x - y);
  return getTheSecondLastMax(maxBidByPlayers);
};

/**
 * Find the winning price givin players regarding the reserve price.
 * @param {Player[]} players List of players trying to win
 * @param {number} reservePrice Minimum price must be paid
 * @return {number} The price to be paid to get the item auction
 */
export const findWinningPriceGivingAReservePrice = (
  players: Player[],
  reservePrice: number
) => {
  const potentialWinningPrice = findWinningPrice(players);
  return potentialWinningPrice && potentialWinningPrice >= reservePrice
    ? potentialWinningPrice
    : reservePrice;
};
