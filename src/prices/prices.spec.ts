import {
  findWinningPriceGivingAReservePrice,
  findWinningPrice,
  getTheSecondLastMax
} from "./prices";

const players = [
  {
    id: "A",
    bids: [110, 50, 100]
  },
  {
    id: "B",
    bids: [120, 140]
  }
];

const reservePrice = 100;

describe("prices module", () => {
  it("should find the second max value equal to 130", () => {
    const maxBidsByPlayers = [50, 115, 125, 130, 140, 140, 140];
    expect(getTheSecondLastMax(maxBidsByPlayers)).toEqual(130);
  });

  it("should find the second max value equal to 130", () => {
    const maxBidsByPlayers = [50, 115, 125, 130, 140, 140];
    expect(getTheSecondLastMax(maxBidsByPlayers)).toEqual(130);
  });

  it("should find the second max value equal to 130", () => {
    const maxBidsByPlayers = [50, 115, 125, 130, 140];
    expect(getTheSecondLastMax(maxBidsByPlayers)).toEqual(130);
  });

  it("should find a winning price equal to 110", () => {
    expect(findWinningPrice(players)).toEqual(110);
  });

  it("should find a winning price equal to 110 givin a reserve price to 100", () => {
    expect(findWinningPriceGivingAReservePrice(players, reservePrice)).toEqual(
      110
    );
  });

  it("should find a winning price equal to 100 givin a reserve price to 100", () => {
    const players = [
      {
        id: "A",
        bids: [80, 50, 20]
      },
      {
        id: "B",
        bids: [120, 140]
      }
    ];
    expect(findWinningPriceGivingAReservePrice(players, reservePrice)).toEqual(
      100
    );
  });
});
